/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to these emails.
 * Open source under GPLv3
 * 
 * version 3.0
 */
import java.net.URL;
import java.util.Properties;

class ResourceLocator {
	// get the path.separator in Windows or Unix like
	public static URL getImgResource(String name){
		return ResourceLocator.class.getResource(name);
	}

}
