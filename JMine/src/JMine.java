/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to these emails.
 * Open source under GPLv3
 * 
 * version 3.0
 */

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class JMine extends JFrame implements MouseListener, ActionListener {
	private JMineArth mine;

	private JMineButton[][] mineButton;

	private GridBagConstraints constraints;

	private JPanel pane;

	private GridBagLayout gridbag;

	private boolean gameStarted;

	private static JCounter mineCounter;

	private static JCounter timeCounter;

	private Timer timer;
	
	private Timer winTimer = new Timer();

	public int numMine;

	public int numFlaged;

	private JMenuBar mb;

	private JMenu mGame;

	private JMenuItem miEasy;

	private JMenuItem miMiddle;

	private JMenuItem miHard;

	private JMenuItem miCustomer;

	private JMenuItem miExit;

	private JMenu mHelp;

	private JMenuItem miAbout;

	private JPanel controlPane;

	private JButton bTest;

	private AboutFrame about;
	
	static WinFrame winFrame;

	static CustomerFrame customerFrame; 
	
	private ImageIcon blankIcon = new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("blank1.gif")),34,34));
	
	private Insets space = new Insets(0, 0, 0, 0);
	
	private final int DEFAULT_COL_COUNT = 10;

	private final int DEFAULT_ROW_COUNT = 10;
	
	public int height;
	
	public int width;
	
	private final int GAME_EASY = 12;

	private final int GAME_HARD = 36;

	private final int GAME_MIDDLE = 24;
	
	private long nowClick = 0L;
	
	private long lastClick = 0L;
	
	private ImageIcon[] mineNumIcon = { new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("blank1.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("1.gif")),34,34)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("2.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("3.gif")),34,34)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("4.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("5.gif")),34,34)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("6.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("7.gif")),34,34)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("8.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("8.gif")),34,34))};

	private ImageIcon[] mineStatus = { new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("blank1.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("flag.gif")),34,34)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("question.gif")),34,34)) };

	private ImageIcon[] mineBombStatus = { new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("0.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("mine.gif")),34,34)), new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("wrongmine.gif")),34,34)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("bomb.gif")),34,34)) };

	private ImageIcon[] faceIcon = { new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("smile.gif")),52,54)),
			new ImageIcon(Util.resizeImage(Util.readImage(ResourceLocator.getImgResource("Ooo.gif")),52,54)) };

	// Constructor of the game
	public JMine() {
		super("JMine Game");
		setSize(500, 700);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Insets space = new Insets(0, 0, 0, 0);

		// Game vars
		gameStarted = false;
		numMine = 12;
		numFlaged = 0;

		ImageIcon myIcon = new ImageIcon(JMine.class.getResource("blank1.gif"));
		gridbag = new GridBagLayout();
		constraints = new GridBagConstraints();
		pane = new JPanel();
		pane.setLayout(gridbag);
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;

		// Begin Menu Set
		mb = new JMenuBar();
		mGame = new JMenu("Game");
		miEasy = new JMenuItem("Easy");
		miEasy.addActionListener(this);
		miMiddle = new JMenuItem("Middle");
		miMiddle.addActionListener(this);
		miHard = new JMenuItem("Hard");
		miHard.addActionListener(this);
		miCustomer = new JMenuItem("Customer");
		miCustomer.addActionListener(this);
		miExit = new JMenuItem("Exit");
		miExit.addActionListener(this);
		mGame.add(miEasy);
		mGame.add(miMiddle);
		mGame.add(miHard);
		mGame.addSeparator();
		mGame.add(miCustomer);
		mGame.addSeparator();
		mGame.add(miExit);
		mb.add(mGame);

		mHelp = new JMenu("Help");
		miAbout = new JMenuItem("About...");
		mHelp.add(miAbout);
		miAbout.addActionListener(this);
		mb.add(mHelp);
		this.setJMenuBar(mb);
		// end of Menu Set

		// Control Panel
		controlPane = new JPanel();
		bTest = new JButton(faceIcon[0]);
		bTest.setSize(52, 54);
		bTest.setMargin(space);
		bTest.addMouseListener(this);
		bTest.setPressedIcon(faceIcon[1]);

		mineCounter = new JCounter(numMine);
		timeCounter = new JCounter();
		controlPane.add(mineCounter);
		controlPane.add(bTest);
		controlPane.add(timeCounter);
		buildConstraints(constraints, 0, 0, 10, 2, 100, 100);
		gridbag.setConstraints(controlPane, constraints);
		pane.add(controlPane);

		width = DEFAULT_COL_COUNT;
		height = DEFAULT_ROW_COUNT;
		// Bottons
		mineButton = new JMineButton[height][width];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				mineButton[i][j] = new JMineButton(i, j, blankIcon);
				mineButton[i][j].addMouseListener(this);
				mineButton[i][j].setMargin(space);
				buildConstraints(constraints, j, i + 3, 1, 1, 80, 80);
				gridbag.setConstraints(mineButton[i][j], constraints);
				pane.add(mineButton[i][j]);
			}
		}

		// Content Pane
		setContentPane(pane);
		setLocation(600, 200);
		setVisible(true);

		// About Frame
		about = new AboutFrame("JMine About");
		about.setLocation(650, 420);
		
		// Customer Frame
		customerFrame = new CustomerFrame("Customer game");
		customerFrame.mineNum = this.numMine;
		customerFrame.colCount = this.width;
		customerFrame.rowCount = this.height;
		customerFrame.setLocation(650, 420);
		
		// Win Frame
		winFrame = new WinFrame("You win!", customerFrame);
		winFrame.setLocation(650, 420);
	}

	public void actionPerformed(ActionEvent e) {
		try {
			if (e.getSource() == miEasy) {
				setNewGame(GAME_EASY,DEFAULT_ROW_COUNT,DEFAULT_COL_COUNT);
				width = DEFAULT_COL_COUNT;
				height = DEFAULT_ROW_COUNT;
				return;
			}
			if (e.getSource() == miMiddle) {
				setNewGame(GAME_MIDDLE,DEFAULT_ROW_COUNT,DEFAULT_COL_COUNT);
				width = DEFAULT_COL_COUNT;
				height = DEFAULT_ROW_COUNT;
				return;
			}
			if (e.getSource() == miHard) {
				setNewGame(GAME_HARD,DEFAULT_ROW_COUNT,DEFAULT_COL_COUNT);
				width = DEFAULT_COL_COUNT;
				height = DEFAULT_ROW_COUNT; 
				return;
			}			
			if (e.getSource() == miCustomer) {
				if(timer!=null) timer.cancel();
				customerFrame.setMineNum(numMine);
				customerFrame.setRowCount(height);
				customerFrame.setColCount(width);
				customerFrame.setOk(false);
				customerFrame.setVisible(false);
				customerFrame.setLocation(650, 420);
				customerFrame.setVisible(true);
				
				winTimer = new Timer();
				
				winTimer.schedule(new TimerTask(){
					@Override
					public void run() {
						//System.out.println("JerryDebug::Width::"+width);
						while(!customerFrame.isOk()){
							try {Thread.sleep(50);}catch(Exception e) {}
						}						
						numMine = customerFrame.getMineNum();
						width = customerFrame.getColCount();
						height = customerFrame.getRowCount();

						setNewGame(numMine,height,width);
						//System.out.println("JerryDebug::Width::"+width);
						winTimer.cancel();
					}
				},0);
				
				return;
			}
			if (e.getSource() == miExit) {
				System.exit(0);
			}
			if (e.getSource() == miAbout) {
				about.setVisible(true);
			}
		} catch (Exception ie) {
		}
	}

	// You lose
	private void bomb(int row, int col, int rowCount, int colCount){
		try{
		//System.out.println("Bomb!");

		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				mineButton[i][j].setIcon(mineBombStatus[0]);
				int toShow;
				toShow = mine.mine[i][j] != 9 ? 0 : 1;
				mineButton[i][j].setClickFlag(true);
				if (toShow == 1 && (i != row || j != col)) {
					mineButton[i][j].setIcon(mineBombStatus[toShow]);
					mineButton[i][j].setClickFlag(true);
				} else if (toShow == 1 && (i == row && j == col)) {
					mineButton[i][j].setIcon(mineBombStatus[3]);
					mineButton[i][j].setClickFlag(true);
				} else if (toShow == 0 && mineButton[i][j].getFlag() != 1) {
					mineButton[i][j].setEnabled(false);
				} else if (toShow == 0 && mineButton[i][j].getFlag() == 1) {
					mineButton[i][j].setIcon(mineBombStatus[2]);
					mineButton[i][j].setClickFlag(true);
				}
			}
		}
		
		timer.cancel();
		
		
		}catch (Exception e){
			
		}
	}
	
	// Set the GUI objects positions
	void buildConstraints(GridBagConstraints gbc, int gx, int gy, int gw,
			int gh, int wx, int wy) {
		gbc.gridx = gx;
		gbc.gridy = gy;
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		gbc.weightx = wx;
		gbc.weighty = wy;
	}

	// the methods to check if there were mines, to be nested
	void checkMine(int row, int col, int rowCount, int colCount){
		int i, j;
		j = col < 0 ? 0 : col;
		j = j > colCount-1 ? colCount-1 : j;
		i = row < 0 ? 0 : row;
		i = i > rowCount-1 ? rowCount-1 : i;
		//System.out.println("Check Mine row:"+i + ",col:" +j);
		if (mine.mine[i][j] == 9) {
			bomb(i, j,rowCount,colCount);
		} else if (mine.mine[i][j] == 0
				&& mineButton[i][j].getClickFlag() == false) {
			mineButton[i][j].setClickFlag(true);
			showLabel(i, j);
			for (int ii = i - 1; ii <= i + 1; ii++)
				for (int jj = j - 1; jj <= j + 1; jj++)
					checkMine(ii, jj, rowCount,colCount);

		} else {
			showLabel(i, j);
			mineButton[i][j].setClickFlag(true);
		}
		if (isWin(rowCount,colCount)) {
			win();
		}
	}
	
	private void clearAll(int row, int col , int rowCount, int colCount ){
		int top, bottom, left, right;
		left = col - 1 > 0 ? col - 1 : 0;
		right = col + 1 < colCount ? col + 1 : colCount - 1;
		top = row - 1 > 0 ? row - 1 : 0;
		bottom = row + 1 < rowCount ? row + 1 : rowCount - 1;
		
		if (countFlagedMine(row, col, rowCount, colCount)==mine.mine[row][col]){
			for (int i = top; i <= bottom; i++) {
				for (int j = left; j <= right; j++) {
					if (mineButton[i][j].getFlag() != 1)
						checkMine(i, j,colCount, rowCount);
				}
			}
		}

	}

	public int countFlagedMine(int row, int col, int rowCount, int colCount){
		int top, bottom, left, right;
		left = col - 1 > 0 ? col - 1 : 0;
		right = col + 1 < colCount ? col + 1 : colCount - 1;
		top = row - 1 > 0 ? row - 1 : 0;
		bottom = row + 1 < rowCount ? row + 1 : rowCount - 1;
		int count = 0;

		for (int i = top; i <= bottom; i++) {
			for (int j = left; j <= right; j++) {
				if (mineButton[i][j].getFlag() == 1)
					count++;
			}
		}
		
		System.out.println("JMine::CountFlagedMine::count::"+ count);
		
		return count;
	}

	// to flag the mine you want to flag out
	void flagMine(int row, int col, int rowCount, int colCount) {
		int i, j;
		j = col < 0 ? 0 : col;
		j = j > colCount-1 ? colCount-1 : j;
		i = row < 0 ? 0 : row;
		i = i > rowCount-1 ? rowCount-1 : i;
		if (mineButton[i][j].getFlag() == 0) {
			numFlaged++;
		} else if (mineButton[i][j].getFlag() == 1) {
			numFlaged--;
		}
		mineCounter.resetCounter(numMine - numFlaged >= 0 ? numMine - numFlaged	: 0);
		mineButton[i][j].setFlag((mineButton[i][j].getFlag() + 1) % 3);
		showFlag(i, j);
		if (isWin(rowCount,colCount)) {
			win();
		}
	}

	// check if you win() {
	private boolean isWin(int rowCount, int colCount) {
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				if (mine.mine[i][j] == 9 && mineButton[i][j].getFlag() != 1) {
					return (false);
				}
				if (mine.mine[i][j] != 9 && mineButton[i][j].getFlag() == 1) {
					return (false);
				}
				if (mine.mine[i][j] != 9
						&& mineButton[i][j].getClickFlag() == false) {
					return (false);
				}
			}
		}
		return true;
	}

	// the event handle to deal with the mouse click
	public void mouseClicked(MouseEvent e) {					
			if (e.getSource() == bTest) {
				setNewGame(numMine,height,width);
				return;
			}else if (e.getSource()==getCustomerFrame().getOk()){
				numMine = getCustomerFrame().getMineNum();
				width = getCustomerFrame().getColCount();
				height = getCustomerFrame().getRowCount();
				setNewGame(numMine,height,width);
				return;
			}
			
			int row,col;
			row = ((JMineButton) e.getSource()).getRow();
			col = ((JMineButton) e.getSource()).getCol();

			if (!gameStarted) {
				startNewGame(numMine, row,col,height,width);
			}
			
			// this block is detect the two mouse button down
			// whether there is two mouse click within 150 micro seconds
			// we will clear all the cells beside this button
			nowClick = System.currentTimeMillis();
			if (lastClick == 0L) {
				lastClick = nowClick;
			}else if(nowClick - lastClick <= 150L){
				lastClick = 0L;
				System.out.println("HA1");
				clearAll(col, row,height,width);
				return;
			}else if(nowClick - lastClick > 150L) {
				lastClick = nowClick;
			}
			
			// the following is detect the second mouse button down
			// if so we regard this same as two mouse button down
			if (e.getButton()==MouseEvent.BUTTON2) {				
				System.out.println("HA");
				clearAll(row, col,height,width);
				return;
			}if (!mineButton[row][col].getClickFlag()) {

				if (e.getModifiers() == InputEvent.BUTTON1_MASK) {
					if (mineButton[row][col].getFlag() == 1) {
						return;
					} else {
						checkMine(row, col, height,width);
					}
				} else if (e.getModifiers() == InputEvent.BUTTON3_MASK) {
					flagMine(row,col,height,width);
				} else {
				}
			}
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
		//System.out.println("Jerry Release");
	}

	// reset the game 
	private void resetAll() {
		mineButton = new JMineButton[height][width];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				mineButton[i][j] = new JMineButton(i, j, blankIcon);
				mineButton[i][j].addMouseListener(this);
				mineButton[i][j].setMargin(space);
				buildConstraints(constraints, j, i + 3, 1, 1, 80, 80);
				gridbag.setConstraints(mineButton[i][j], constraints);
				pane.add(mineButton[i][j]);
			}
		}
	}
	
	// circle the flag with blank, flaged, questioned
	void showFlag(int row, int col) {
		mineButton[row][col]
				.setIcon(mineStatus[mineButton[row][col].getFlag()]);
	}

	// show the numbers of the nearby mines
	void showLabel(int row, int col) {
		//System.out.println("ShowLabel row:" + row + ",col:" + col);
		int toShow;
		toShow = mine.mine[row][col];
		if (toShow != 0) {
			mineButton[row][col].setIcon(mineNumIcon[toShow]);
			mineButton[row][col].setClickFlag(true);
		} else {
			mineButton[row][col].setIcon(mineNumIcon[0]);
			mineButton[row][col].setEnabled(false);
		}
	}

	// method to start the new game
	private void startNewGame(int num, int row, int col, int rowCount, int colCount){
		mine = new JMineArth(num, row, col, rowCount, colCount);
		mine.printMine();
			
		gameStarted = true;
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask(){
			public void run() {
				timeCounter.counterAdd();
			}
		},1000,1000);
		
	}
		
	// You Win
	private void win(){		
		timer.cancel();
		winFrame.setVisible(false);
		winFrame.setLocation(650, 420);
		winFrame.customerFrame.setLocation(650, 420);
		winFrame.setVisible(true);

		winTimer = new Timer();
		winTimer.schedule(new TimerTask(){
			@Override
			public void run() {				
				while(!winFrame.getWinOk()){
					try {Thread.sleep(50);}catch(Exception e) {}
				}
				if (winFrame.isCustomer()){
					numMine = winFrame.getCustomerFrame().getMineNum();
					height = winFrame.getCustomerFrame().getRowCount();
					width = winFrame.getCustomerFrame().getColCount();
					setNewGame(numMine,height,width);
				}else {
					numMine = winFrame.getMineNum();
					setNewGame(numMine,height,width);				
				}
				winFrame.setLocation(650,420);
				winFrame.setVisible(false);
				winFrame.setWinOk(false);
				winTimer.cancel();
			}
		},0L);

	}

	public CustomerFrame getCustomerFrame() {
		return customerFrame;
	}

	public void setCustomerFrame(CustomerFrame customerFrame) {
		this.customerFrame = customerFrame;
	}
	
	public void setNewGame(int num, int rowCount, int colCount) {
		resetAll();
		numMine = num;
		numFlaged = 0;
		gameStarted = false;
		mineCounter.resetCounter(numMine);
		timeCounter.resetCounter(0);
		if (timer != null) timer.cancel();

		setSize(500+(colCount-10)*48, 700 +(rowCount-10)*48);
		setLocation(600-(colCount-10)*12, 200-(colCount-10)*12);
		pane.removeAll();	
		
		// Control Panel
		controlPane = new JPanel();
		bTest = new JButton(faceIcon[0]);
		bTest.setSize(52, 54);
		bTest.setMargin(space);
		bTest.addMouseListener(this);
		bTest.setPressedIcon(faceIcon[1]);

		mineCounter = new JCounter(numMine);
		timeCounter = new JCounter();
		controlPane.add(mineCounter);
		controlPane.add(bTest);
		controlPane.add(timeCounter);
		buildConstraints(constraints, 0, 0, colCount, 2, 100, 100);
		gridbag.setConstraints(controlPane, constraints);
		pane.add(controlPane);
		
		mineButton = new JMineButton[rowCount][colCount];
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				mineButton[i][j] = new JMineButton(i, j, blankIcon);
				mineButton[i][j].addMouseListener(this);
				mineButton[i][j].setMargin(space);
				buildConstraints(constraints, j, i + 3, 1, 1, 80, 80);
				gridbag.setConstraints(mineButton[i][j], constraints);
				pane.add(mineButton[i][j]);
			}
		}
		
		setContentPane(pane);
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String [] args) {
		JMine jmine = new JMine();
		jmine.setVisible(true);
	}
}


