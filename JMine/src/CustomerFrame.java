/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to these emails.
 * Open source under GPLv3
 * 
 * version 3.0
 */
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
class CustomerFrame extends JFrame implements MouseListener,ActionListener {

	private JLabel widthLabel;

	private JLabel heightLabel;

	private JLabel mineNumLabel;

	private JTextField widthField;

	private JTextField heightField;

	private JTextField mineNumField;

	private JPanel panel;

	private GridBagLayout gridbag;

	private GridBagConstraints constrains;

	private JButton ok;

	private JButton reset;
	
	static int colCount;
	
	static int rowCount;
	
	static int mineNum;
	
	private boolean isOk;
			
	public JButton getOk() {
		return ok;
	}

	public void setOk(JButton ok) {
		this.ok = ok;
	}

	public CustomerFrame(String name) {
		super(name);
		setSize(360, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		gridbag = new GridBagLayout();
		constrains = new GridBagConstraints();
		panel.setLayout(gridbag);
		

		widthLabel = new JLabel("Width:");		
		buildConstraints(constrains, 0, 0);
		panel.add(widthLabel,constrains);				

		widthField = new JTextField();
		widthField.setColumns(6);
		widthField.setText("10");
		buildConstraints(constrains, 1, 0);
		panel.add(widthField, constrains);

		heightLabel = new JLabel("Height:");		
		buildConstraints(constrains, 0, 1);
		panel.add(heightLabel,constrains);	
		
		heightField = new JTextField();
		heightField.setColumns(6);
		heightField.setText("10");
		buildConstraints(constrains, 1, 1);
		panel.add(heightField,constrains);

		mineNumLabel = new JLabel("Mine Number:");		
		buildConstraints(constrains, 0, 2);
		panel.add(mineNumLabel, constrains);		

		mineNumField = new JTextField();
		mineNumField.setColumns(6);
		mineNumField.setText("12");
		buildConstraints(constrains, 1, 2);
		panel.add(mineNumField, constrains);	
		
		ok = new JButton("OK");	
		ok.addMouseListener(this);
		buildConstraints(constrains, 0, 3);
		panel.add(ok, constrains);
		
		reset = new JButton("Reset");
		reset.addMouseListener(this);
		buildConstraints(constrains, 1, 3);
		panel.add(reset,constrains);

		setLocation(650, 420);
		setContentPane(panel);
	}

	public static void main(String[] args) {
		CustomerFrame cf = new CustomerFrame("Test");
		cf.setVisible(true);
	}

	// Set the GUI objects positions
	private void buildConstraints(GridBagConstraints gbc, int gx, int gy) {
		gbc.gridx = gx;
		gbc.gridy = gy;
	}

	// the event handle to deal with the mouse click
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == ok) {
			try {
				int width = Integer.parseInt(widthField.getText().trim());
				int height = Integer.parseInt(heightField.getText().trim());
				int mineNum = Integer.parseInt(mineNumField.getText().trim());
				if (width <10 || width > 40){
					JOptionPane.showMessageDialog(this, "JMine width error.");
					this.setOk(false);
					return;
				}
				else if (height <10 || height > 20){
					JOptionPane.showMessageDialog(this, "JMine hight error");
					this.setOk(false);
					return;
				}
				else if (mineNum <=0 || mineNum>= height* width){
					JOptionPane.showMessageDialog(this, "JMine mine number error");
					this.setOk(false);
					return;
				}
				this.colCount = width;
				this.rowCount = height;
				this.mineNum = mineNum;
				this.setOk(true);
				this.setVisible(false);			
			}
			catch (Exception ex){
				this.setOk(false);
				JOptionPane.showMessageDialog(this, "Please input number in the text field.");
			}
		}
		else if (e.getSource() == reset) {
			heightField.setText("");
			widthField.setText("");
			mineNumField.setText("");
		}

	}

	public void mousePressed(MouseEvent e) {
		// System.out.println("Jerry Press");

	}

	public void mouseReleased(MouseEvent e) {
		// System.out.println("Jerry Release");
	}

	public void mouseExited(MouseEvent e) {
		// System.out.println("Jerry Exited");

	}

	public void mouseEntered(MouseEvent e) {
		// System.out.println("Jerry Entered");
	}

	public int getColCount() {
		return Integer.parseInt(widthField.getText());
	}

	public int getRowCount() {
		return Integer.parseInt(heightField.getText());
	}

	public int getMineNum() {
		return Integer.parseInt(mineNumField.getText());
	}
	
	public boolean isOk(){
		return isOk;
	}

	public void setOk(boolean ok){
		this.isOk = ok;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == ok) {
			try {
				int width = Integer.parseInt(widthField.getText().trim());
				int height = Integer.parseInt(heightField.getText().trim());
				int mineNum = Integer.parseInt(mineNumField.getText().trim());
				if (width <10 || width > 40){
					JOptionPane.showMessageDialog(this, "JMine width error.");
					this.setOk(false);
					return;
				}
				else if (height <10 || height > 20){
					JOptionPane.showMessageDialog(this, "JMine hight error");
					this.setOk(false);
					return;
				}
				else if (mineNum <=0 || mineNum>= height* width){
					JOptionPane.showMessageDialog(this, "JMine mine number error");
					this.setOk(false);
					return;
				}
				this.colCount = width;
				this.rowCount = height;
				this.mineNum = mineNum;
				this.setOk(true);
				this.setVisible(false);			
			}
			catch (Exception ex){
				this.setOk(false);
				JOptionPane.showMessageDialog(this, "Please input number in the text field.");
			}
		}
		else if (e.getSource() == reset) {
			heightField.setText(""+rowCount);
			widthField.setText(""+colCount);
			mineNumField.setText(""+mineNum);
		}
	}

	public void setColCount(int colCount) {
		CustomerFrame.colCount = colCount;
		widthField.setText(""+colCount);
	}

	public void setRowCount(int rowCount) {
		CustomerFrame.rowCount = rowCount;
		heightField.setText(""+rowCount);
	}

	public void setMineNum(int mineNum) {
		CustomerFrame.mineNum = mineNum;
		mineNumField.setText(""+mineNum);
	}

}
