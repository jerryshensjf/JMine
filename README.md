# 开源Java 扫雷游戏JMine

### 项目介绍
开源Java 扫雷游戏JMine，采用Swing界面，是可执行的Jar，在安装好Java运行环境的计算机上，只要双击即可运行。此处提供1.2.5和2.0的可执行jar包和源码下载。

此软件是笔者开源Python扫雷游戏PyMine的祖先。

JMine 3.0正在主分支上开发。此版本迁徙到open jdk 17。解决了JMine 2.0的行列反置问题，支持自定义游戏。为了适配新的计算机的视觉分辨率的提升，所有图片均放大四倍。为了适配现在的计算机的高速度，所有工作线程中均设置了短时睡眠，以便线程响应。

截图（JMine 1.2.5）

![输入图片说明](https://images.gitee.com/uploads/images/2018/1017/213911_5c29f9ad_1203742.png "jmine.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1017/214023_c0cc0812_1203742.png "jmine_2.png")

#### JMine 3.0截图

![输入图片说明](JMine30.png)

### 软件架构
软件架构说明
Java， Swing

### 安装教程

1. 装好JRE或JDK
2. 双击执行